# Helpful Git Stuff
Here are some hopefully helpful git stuff you may want.  I cannot promise any of these command work perfectly, I'm no git wizard. 

### Getting the most recent changes
###### Disclaimer
Merging is sometimes a pain so if you are not comforatble doing it, copying and pasting is totally okay. 

First set up a remote branch. This only needs to be done once.
```sh
git remote add cse181-codebase https://git.ucsc.edu/malanthi/cse181-codebase.git
```
Then fetch those changes whenever you want the most recent codebase. 
```sh
git fetch cse181-codebase
```
Then merge them.
```sh
git merge cse181-codebase/master
```
You may have some merge conflicts that you will have to resolve.  


